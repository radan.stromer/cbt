    <?php
@session_start();
include "+koneksi.php";

$id_tq = @$_GET['id_tq'];
$no = 1;
$no2 = 1;
$sql_tq = mysqli_query($db, "SELECT * FROM tb_topik_quiz JOIN tb_mapel ON tb_topik_quiz.id_mapel = tb_mapel.id WHERE id_tq = '$id_tq'") or die ($db->error);
$data_tq = mysqli_fetch_array($sql_tq);
?>
<script src="style/assets/js/jquery-1.11.1.js"></script>
<script src="style/assets/js/bootstrap.js"></script>
<script src="assets/js/jquery.magnific-popup.js"></script>
<script>
var waktunya;
waktunya = <?php echo $data_tq['waktu_soal']; ?>;
var waktu;
var jalan = 0;
var habis = 0;

function init(){
    checkCookie();
    mulai();
}
function keluar(){
    if(habis==0){
        setCookie('waktux',waktu,365);
    }else{
        setCookie('waktux',0,-1);
    }
}
function mulai(){
    jam = Math.floor(waktu/3600);
    sisa = waktu%3600;
    menit = Math.floor(sisa/60);
    sisa2 = sisa%60
    detik = sisa2%60;
    if(detik<10){
        detikx = "0"+detik;
    }else{
        detikx = detik;
    }
    if(menit<10){
        menitx = "0"+menit;
    }else{
        menitx = menit;
    }
    if(jam<10){
        jamx = "0"+jam;
    }else{
        jamx = jam;
    }
    document.getElementById("divwaktu").innerHTML = jamx+" Jam : "+menitx+" Menit : "+detikx +" Detik";
    waktu --;
    if(waktu>0){
        t = setTimeout("mulai()",1000);
        jalan = 1;
    }else{
        if(jalan==1){
            clearTimeout(t);
        }
        habis = 1;
        $('<input>').attr({
            type: 'hidden',
            name: 'habis',
            value: '1'
        }).appendTo('form');
        $("input[value='selanjutnya']").remove();
        document.getElementById("kirim").click();
    }
}
function selesai(){    
    if(jalan==1){
        clearTimeout(t);
    }
    habis = 1;
}
function getCookie(c_name){
    if (document.cookie.length>0){
        c_start=document.cookie.indexOf(c_name + "=");
        if (c_start!=-1){
            c_start=c_start + c_name.length+1;
            c_end=document.cookie.indexOf(";",c_start);
            if (c_end==-1) c_end=document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}
function setCookie(c_name,value,expiredays){
    var exdate=new Date();
    exdate.setDate(exdate.getDate()+expiredays);
    document.cookie=c_name+ "=" +escape(value)+((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
}
function checkCookie(){
    waktuy=getCookie('waktux');
    if (waktuy!=null && waktuy!=""){
        waktu = waktuy;
    }else{
        waktu = waktunya;
        setCookie('waktux',waktunya,7);
    }
}
</script>
<script type="text/javascript">
    window.history.forward();
    function noBack(){ window.history.forward(); }
</script>

<?php
if(@$_SESSION['siswa']) { ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Ujian Online E-Learning <?=NAMA_SEKOLAH;?></title>
    <link href="style/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="style/assets/css/font-awesome.css" rel="stylesheet" />
    <link href="style/assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/magnific-popup.css" rel="stylesheet" />
    <style type="text/css">
    .mrg-del {
        margin: 0;
        padding: 0;
    }
    </style>
</head>
<body onload="init(),noBack();" onpageshow="if (event.persisted) noBack();" onunload="keluar()">

<!-- <div class="navbar navbar-inverse set-radius-zero">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./">
                <h4 style="color: white;"><?=NAMA_SEKOLAH;?></h4>
                    <h5 style="color: white;"><?=ALAMAT_SEKOLAH;?></h5>
            </a>
        </div>

        <div class="left-div">
            <div class="user-settings-wrapper">
                <ul class="nav">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                            <span class="glyphicon glyphicon-user" style="font-size: 25px;"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div> -->

<div class="content-wrapper">
    <div class="container">
		<div class="row">
		    <div class="col-md-12">
		        <h4 style="float:left;font-weight: 900;
    text-transform: uppercase;
    color: #F0677C;
    font-size: 20px;
    ">
                    <span>Mapel : <?php echo $data_tq['mapel']; ?></span></h4>

                <b style="margin-top:10px;float:right; text-align: center;   font-size: 16px;">Info <small>(Sisa waktu Anda)</small>
                    <span id="divwaktu" style="    color: white;
    background: blue;
    padding: 5px;"></span>
                </b>
		    </div>
             <div class="panel panel-default">
                   
                    <div class="panel-body">
                        <h3 align="center"><span id="divwaktu"></span></h3>
                    </div>
                </div>
		</div>

		<div class="row">
            <div class="col-md-10" style="padding: 0px;">
                <?php include "kanan_soal.php";?>
            </div>
            <div class="col-md-2" style="padding-left:  2px; padding-right: 0px;">
            <!-- <a class="btn btn-primary pull-right" href="#" id="hide-answer" style="top:25px; position: absolute;right: 0px;">Lihat Jawaban</a> -->
			
		       
                <?php $sql_soal_sudah_jawab = mysqli_query($db, "SELECT 
                    tb_jawaban_pilgan_temp.id_soal,
                    tb_jawaban_pilgan_temp.jawaban
                    FROM tb_jawaban_pilgan_temp 
                    #LEFT JOIN tb_soal_pilgan ON tb_soal_pilgan.id_pilgan = tb_jawaban_pilgan_temp.id_soal
                    WHERE id_peserta = '".$_SESSION['siswa']."' AND id_tq = '{$id_tq}'") or die ($db->error);
                    $soal_sudah_jawab = mysqli_num_rows($sql_soal_sudah_jawab);
                    if($soal_sudah_jawab>0) { 
                ?>
                <!-- <div class="panel panel-default">
                    <div class="panel-heading"><b>Butir Soal</small></b></div>
                    <div class="panel-body">
                        <div class="list-group">
                            <?php $no=1; foreach($sql_soal_sudah_jawab as $soal){?>
                                <a href="soal.php?id_tq=<?php echo $id_tq;?>&revisi_soal=<?php echo $soal['id_soal'];?>&no_revisi=<?php echo $no;?>" class="list-group-item"><?php echo $no.")".$soal['jawaban'];?></a>
                            <?php $no++;}?>
                        </div>
                    </div>
                </div> -->
                <style type="text/css">
                    /*Mengatur lebar tombol nomor soal*/
                    .blok-nomor{
                       width: 29%;
                       display: inline-block;
                    }
                    .blok-nomor .box{
                       padding: 5px;
                       
                    }

                    /*Mengatur desain tombol nomor soal*/
                    .tombol-nomor{
                       display: block;
                       width: 100%;
                       text-align: center;
                       background: red;
                       color: #fff;
                       border: 2px solid #000;
                       cursor: pointer;
                       font-size: 16px;
                    }
                    /*Mengatur warna tombol nomor soal*/
                    .tombol-nomor.green{
                        background: green;
                    }
                    .tombol-nomor.yellow{
                        background: red;
                        color: white;
                    }
                    
                </style>
                <div class="nomor-ujian">
                    <?php $no=1; foreach($sql_soal_sudah_jawab as $soal){ 
                        if(!empty($soal['jawaban'])) $warna_tombol = "tombol-nomor green";
                        else $warna_tombol = "tombol-nomor yellow";
                    ?>
                    <div class="blok-nomor">
                        <div class="box">
                            <a class="tombol-nomor <?=$warna_tombol;?>" href="soal.php?id_tq=<?php echo $id_tq;?>&revisi_soal=<?php echo $soal['id_soal'];?>&no_revisi=<?php echo $no;?>">
                                <?php echo $no.") ".$soal['jawaban'];?>
                            </a>
                        </div>
                    </div>
                    <?php  $no++; }?>
                </div>
                <?php }?>
		    </div>

		    
		</div>

	</div>
</div>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                &copy; <?=date('Y');?> CBT Software | By : <?=COMPANY;?>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript">
    $('.img-soal').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        }
    });

    $("#hide-answer").click(function(){
        $(".nomor-ujian").stop().toggle('slide');
    });
</script>
</body>
</html>
<?php } else {
	echo "<script>window.location='./';</script>";
}?>